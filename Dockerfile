FROM alpine:latest

# In case no version is passed
ARG version=1.0

COPY app.go /app
EXPOSE 8080
ENTRYPOINT ["/app"]
