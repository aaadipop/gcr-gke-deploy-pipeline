## GCR-GKE Pipeline

**gitlab-ci.yml** defines a pipeline for test, build, push image to Google Container Registry and deploy apps to Google Kubernetes Engine.

More info's here -> https://iservit.ro/tutorial/gitlab-ci-cd-pipeline-push-image-to-grc-and-cannary-deploy-to-gke-gcp-infrastructure-overview/

Contains only k8s Deployment. Services and other components are detailed here: https://iservit.ro/tutorial/gcp-infrastucture-overview/
